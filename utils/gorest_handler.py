import requests


class GoRESTHandler:
    base_url = "https://gorest.co.in/public/v2"
    users_endpoint = "/users"

    headers = {
        "Authorization": "Bearer 744954756497228c3d701e94ed266e4097d4479b3dea143cbca973057312f2cc"
    }

    def create_user(self, user_data, expected_status_code=201):
        response = requests.post(self.base_url + self.users_endpoint, json=user_data, headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def get_user(self, user_id):
        response = requests.get(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 200
        return response

    def update_user_status(self, user_data):
        response = requests.patch(self.base_url + self.users_endpoint, params={user_data["status"]: "inactive"})
        assert response.status_code == 200
        return response

    def delete_user(self, user_id, expected_status_code=404):
        response = requests.delete(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == expected_status_code
        return response
    
