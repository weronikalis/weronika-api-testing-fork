import requests
from datetime import datetime

format = "%Y/%m/%d"


def test_default_beers():
    response = requests.get("https://api.punkapi.com/v2/beers")
    body = response.json()
    assert body[0]["id"] == 1
    assert body[-1]["id"] == 25
    assert len(body) == 25


def test_ids_11_to_20():
    params = {
        "ids": "11|12|13|14|15|16|17|18|19|20"
    }
    response = requests.get("https://api.punkapi.com/v2/beers", params=params)
    body = response.json()
    assert len(body) == 10

    beer_id = 11
    for beer in body:
        assert beer["id"] == beer_id
        beer_id += 1


# Pobierz piwo o id=123
def test_id_123():
    response = requests.get("https://api.punkapi.com/v2/beers/123")
    body = response.json()
    for beer in body:
        assert beer["id"] == 123
        assert beer["name"] == "Candy Kaiser"
    assert len(body) == 1


# Pobierz 20 wyników z 5-tej strony
def test_20_results_from_page_5():
    response = requests.get("https://api.punkapi.com/v2/beers?page=5&per_page=20")
    body = response.json()
    assert len(body) == 20
    assert body[0]["id"] == 81

    beer_id = 81
    for beer in body:
        assert beer["id"] == beer_id
        beer_id += 1


# Pobierz wyniki o wartości abv z zakresu <5, 7>
def test_for_abv_from_5_to_7():
    response = requests.get("https://api.punkapi.com/v2/beers?abv_gt=4.9&abv_lt=7.1")
    body = response.json()
    for beer in body:
        beer_id = 5
        assert 4.9 < beer["abv"] < 7.1
    beer_id += 1


# Pobierz piwa wyprodukowane w 2010 roku
def test_for_all_beers_produced_in_2010():
    response = requests.get("https://api.punkapi.com/v2/beers?brewed_before=01-2011&brewed_after=12-2009")
    body = response.json()
    for beer in body:
        assert beer[
                   "first_brewed"] is "01/2010" or "02/2010" or "03/2010" or "04/2010" or "05/2010" or "06/2010" or "07/2010" or "08/2010" or "09/2010" or "10/2010" or "11/2010" or "12/2010"
