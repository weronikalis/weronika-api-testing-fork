import requests
from poke_api_class_handler import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()


def test_quantity_of_pokemons_and_if_body_not_empty():
    """Pokemon API: assert/api/v2/pokemon/ response is not empty"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon/?offset=0&limit=10000")
    body = response.json()
    assert response.status_code == 200  # Test code is 200
    assert len(body["results"]) == 1279  # There are 1279 Pokemons on the list
    assert len(body) is not 0  # Body is not empty


def test_size_of_response_is_under_100_kb():
    """Pokemon API: assert/api/v2/pokemon/ response size is under 100 kb"""
    response = pokeapi_handler.get_list_of_pokemons()
    response_body = response.json()

    assert len(response_body["results"]) > 0
    assert response_body["results"]

    # additional option of test
    response_size_kb = len(response.content) / 1000
    assert response_size_kb < 100


def test_time_response_pokeapi():
    """Pokemon API: assert/api/v2/pokemon/ response time is under 1s"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    time = requests.get("https://pokeapi.co/api/v2/pokemon/").elapsed.total_seconds()
    assert time < 1

    # additional option of test
    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000


def test_offset_20_and_limit_10():
    response = requests.get("https://pokeapi.co/api/v2/pokemon/?offset=20&limit=10")
    response_body = response.json()

    assert len(response_body["results"]) == 10
    assert response_body["results"][0]["name"] == "spearow"
    assert response_body["results"][-1]["name"] == "nidorina"

    # additional option of test


def test_pagination():
    params = {
        "limit": 11,
        "offset": 21
    }

    response = pokeapi_handler.get_list_of_pokemons(params)
    body = response.json()

    assert len(body["results"]) == params["limit"]

    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + 1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + params['limit']}/"
